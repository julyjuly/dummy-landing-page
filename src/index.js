// Import CSS
import "./css/main.css";

// Import js
import "./scripts/libs/modernizr";

import "./scripts/modules/hero";
import "./scripts/modules/navigation";