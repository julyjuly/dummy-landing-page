import $ from "jquery";

$.extend($.easing, {
  easeInOutExpo: function(x, t, b, c, d) {
    if (t == 0) return b;
    if (t == d) return b + c;
    if ((t /= d / 2) < 1) return (c / 2) * Math.pow(2, 10 * (t - 1)) + b;
    return (c / 2) * (-Math.pow(2, -10 * --t) + 2) + b;
  },
});

const body = $('body'),
      hero = $('.js-hero'),
      videoWrapper = hero.find('.js-hero-embed'),
      videoIframe = videoWrapper.find('iframe');

/* ==========================================================
 * Add animations
 * ========================================================== */
$(function () {
  $('body').addClass('animation-in');
});

/* ==========================================================
 * Hero scroll indicator: Scroll to bottom of hero on click
 * ========================================================== */
$(document).on("click", ".js-hero-scroll", function(e) {
  e.preventDefault();

  $("html, body").animate(
    {
      scrollTop: $(".js-hero")[0].scrollHeight,
    },
    1200,
    "easeInOutExpo"
  );
});

/* ==========================================================
 * Hero: Video player
 * ========================================================== */

//  Play video function
const playVideo = function () {

  // Wrapper class
  hero.removeClass('hero-video-stop');
  hero.addClass('hero-video-playing');

  body.removeClass('body-hero-video-stop');
  body.addClass('stop-scroll body-hero-video-playing');

  // If iframe exist
  if (videoIframe.length) {
    setTimeout(function () {
      videoIframe.attr('src', videoIframe.attr('data-src'));
    }, 600);
  }
};

// Stop video function
const stopVideo = function (timeout = 1000) {

  hero.addClass('hero-video-stop');
  hero.removeClass('hero-video-playing');

  body.addClass('body-hero-video-stop');
  body.removeClass('body-hero-video-playing stop-scroll');

  setTimeout(function () {
    body.removeClass('body-hero-video-stop');
  }, 2000);

  // If iframe exist
  if (videoIframe.length) {

    setTimeout(function () {
      videoIframe.attr('src', '');
    }, timeout);
  }
};

// On video play btn click
$('.js-play-hero-embed').on('click', playVideo);

// On video stop btn click
$('.js-stop-hero-embed').on('click', stopVideo);

// Stop video on ESC key
document.addEventListener('keydown', function (ev) {
  let keyCode = ev.keyCode || ev.which;
  if (keyCode === 27 && hero.hasClass('hero-video-playing')) {
    stopVideo();
  }
});