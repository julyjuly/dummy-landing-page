import $ from "jquery";

/* ==========================================================
 * Navigation
 * ========================================================== */
$(function () {

  let bodyEl = $('body'),
      siteContent = $('.site-wrapper'),
      navigationTrigger = $('.js-trigger-site-navigation'),
      navigationHamburger = $('.js-trigger-site-navigation .hamburger'),
      navigationTextOpen = $('.js-trigger-site-navigation .navigation-trigger__text--open'),
      navigationTextClose = $('.js-trigger-site-navigation .navigation-trigger__text--close');

  const toggleNav = function () {
    bodyEl.toggleClass('site-nav-is-open');
    bodyEl.toggleClass('stop-scroll');
    navigationTrigger.toggleClass('is-active');
    navigationHamburger.toggleClass('is-active');
    navigationTextOpen.toggleClass('is-active');
    navigationTextClose.toggleClass('is-active');
  };

  // Open/close lateral navigation
  $('.js-trigger-site-navigation').on('click', function (event) {
    event.preventDefault();
    toggleNav();
  });

  // Close navigation if the clicked target is not the navigation or one of its descendants.
  siteContent.on('click', function (event) {
    var target = event.target;
    if (document.body.classList.contains('site-nav-is-open') && target !== navigationTrigger) {
      toggleNav(event);
    }
  });

  // esc key closes search overlay
  // keyboard navigation events
  document.addEventListener('keydown', function (event) {
    var keyCode = event.keyCode || event.which;
    if (keyCode === 27 && (document.body.classList.contains('site-nav-is-open'))) {
      toggleNav(event);
    }
  });

});